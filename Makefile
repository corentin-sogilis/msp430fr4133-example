# SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS
#
# Makefile : Build basic blink example for msp430fr4133
# Author : Corentin <corentin@sogilis.com>, Alex <alexandre@sogilis.com>
#
# SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS

DOCKER   = docker run --rm --privileged --name devkit-msp430fr4133-instance \
                  -v $(shell pwd):/opt/workspace -it \
                  sogicoco/msp430fr4133-devkit
DOCKERX  = docker exec -it devkit-msp430fr4133-instance
CC       = $(DOCKER) msp430-elf-gcc -mmcu=msp430fr4133 -I /opt/include/ -L /opt/lib/
MSPDEBUG = $(DOCKER) mspdebug tilib
GDB      = $(DOCKERX) msp430-elf-gdb
ELF      = firmware.elf
CFLAGS   = -g

.PHONY : all main flash debug clean gdb-server gdb

all : main

main :
	$(CC) $(CFLAGS) main.c -o $(ELF)

flash : $(ELF) main
	$(MSPDEBUG) 'prog $<'

fw-update :
	$(MSPDEBUG) --allow-fw-update

gdb-server : main
	$(MSPDEBUG) 'gdb'

gdb :
	$(GDB) --eval-command="target remote localhost:2000" $(ELF)

clean :
	rm -rf $(ELF)
