/**
 * SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS
 *
 * main.c : Basic program to blink the green (P4) led of
 *          the MSP430FR4133
 * Author : corentin <corentin@sogilis.com>,
 *          alex <alexendre@sogilis.com,
 *          from various Internet sources
 *
 * SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS
 */

#include <msp430.h>

int main(int argc, char *argv[])
{
    /* Hold the watchdog */
    WDTCTL = WDTPW + WDTHOLD;

    /* Set P4.0 direction to output */
    P4DIR |= 0x01;

    /* Set P4.0 output high */
    P4OUT |= 0x01;

    PM5CTL0 &= ~LOCKLPM5;

    while (1) {
        /* Wait for 400000 cycles */
        __delay_cycles(400000);

        /* Toggle P4.0 output */
        P4OUT ^= 0x01;
    }
}
